import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-custom-form',
  templateUrl: './custom-form.component.html',
  styleUrls: ['./custom-form.component.scss']
})
export class CustomFormComponent implements OnInit {

  public mapsImages: string[] = [];
  private staticImageUrl = 'https://maps.googleapis.com/maps/api/staticmap?size=400x400&maptype=satellite&key=AIzaSyCmZ4JG7DQKOhf_2mmxj8nxyiZo0ZRtl6Y&&path=color:0x00000000|weight:5|fillcolor:0xFFFF0033';

  constructor() { }

  ngOnInit() {

  }

  onFileChange(event: any) {

    let file = event.target.files[0];

    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {

      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary', sheetStubs: true, cellDates: true });
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      let fileData = XLSX.utils.sheet_to_json(ws, { header: 2 });

      let re = /^([-+]?\d*\.?\d*)\s([-+]?\d*\.?\d*)$/;
      fileData.forEach(element => {

        element['Polygons'] = element['Polygons'].replace('Polygon ((', '').replace('))', '');

        let polygonsArray = element['Polygons'].split(',');
        let extraPath = '';

        polygonsArray.forEach((item, index) => {

          let pathPart = item.replace(re, "$2, $1").replace(/ +/g, "");
          extraPath += '|' + pathPart;
        });

        let fullPath = this.staticImageUrl + extraPath;
        this.mapsImages.push(fullPath);
      });

    };
    reader.readAsBinaryString(file);
  }

}
