import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class GoogleMapsStaticApiService {

    private API_ENDPOINT = 'https://maps.googleapis.com/maps/api/staticmap';
    // private postRequestOptions: RequestOptions;
    // private getRequestOptions: RequestOptions;

    constructor(private http: Http) {
        // let headerOptions = new Headers({ 'Content-Type': 'application/json' });
        // this.postRequestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        // this.getRequestOptions = new RequestOptions();

    }

    getSatelliteImage(polygon?: any): Observable<any> {
        let headerOptions = new Headers({ 'Content-Type': 'application/json' });

        let params = new URLSearchParams();

        params.append('size', '300x300');
        params.append('maptype', 'satellite');
        params.append('path', 'color:0x00000000|weight:5|fillcolor:0xFFFF0033|41.43405849093977,-94.68423501669172|41.43378267781478,-94.68423431530874|41.43328785370516,-94.68332775081457|41.43346993346853,-94.68312026534493|41.433952052212746,-94.6832420339325|41.43405849093977,-94.68423501669172');
        params.append('key', 'AIzaSyCmZ4JG7DQKOhf_2mmxj8nxyiZo0ZRtl6Y');


        let reqOptions = new RequestOptions({ params: params });
        // let reqOptions = new RequestOptions({ headers: headerOptions, params: params });
        // reqOptions.params = params;

        return this.http.get(`${this.API_ENDPOINT}`, reqOptions)
            .pipe(map(response => response.json()));
    };

}
