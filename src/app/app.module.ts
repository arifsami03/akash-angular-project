import { BrowserModule } from '@angular/platform-browser';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { HttpModule } from '@angular/http';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import {FlexLayoutModule} from '@angular/flex-layout';

import { GoogleMapsStaticApiService } from './services/google-maps-static-api-service';

import { AppComponent } from './app.component';
import { CustomFormComponent } from './custom-form/custom-form.component';

@NgModule({
  declarations: [
    AppComponent,
    CustomFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    MDBBootstrapModule.forRoot(),
    FlexLayoutModule
  ],
  schemas: [NO_ERRORS_SCHEMA],
  providers: [GoogleMapsStaticApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
